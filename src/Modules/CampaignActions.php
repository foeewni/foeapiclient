<?php
/**
 * Created by PhpStorm.
 * User: simon.phillips
 * Date: 24/04/15
 * Time: 08:59
 */

namespace Foe\Api\Client\Modules;


class CampaignActions extends BaseModule{

    public function getTopics()
    {
        return $this->api->doGet('campaignactions/topics');
    }

    public function getCounterForTopic($topic)
    {
        // TODO: encode topic?
        return $this->api->doGet("campaignactions/topics/$topic/counter");
    }

    public function getCountersForAll()
    {
        return $this->api->doGet("campaignactions/all/counter");
    }

    public function getCounterForAction($id)
    {
        return $this->api->doGet("campaignactions/$id/counter");
    }

    public function getTargetsForPostcode($targetType, $postcode)
    {
        // TODO: encode postcode
        return $this->api->doGet("campaignactions/targets/$targetType/$postcode");
    }

    public function createAction(array $params)
    {
        // TODO
        return $this->api->doPost("campaignactions/",$params);
    }

    public function updateAction($id, $params)
    {
        // TODO
        return $this->api->doPost("campaignactions/$id",$params);
    }
} 