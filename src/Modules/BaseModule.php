<?php
/**
 * Created by PhpStorm.
 * User: simon.phillips
 * Date: 24/04/15
 * Time: 08:59
 */

namespace Foe\Api\Client\Modules;

use Foe\Api\Client\FoeApi;

abstract class BaseModule {
    /**
     * @var FoeApi
     */
    protected $api;

    public function __construct(FoeApi $parentApi)
    {
        $this->api = $parentApi;
    }
} 