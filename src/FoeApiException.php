<?php
/**
 * Created by PhpStorm.
 * User: simon.phillips
 * Date: 24/04/15
 * Time: 08:42
 */

namespace Foe\Api\Client;


class FoeApiException extends \Exception{

    protected $type;
    protected $message;

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->message;
    }

    /**
     * @return string|int
     */
    public function getType()
    {
        return $this->type;
    }

    public function __construct($type, $message, $correlationId)
    {
        $this->type = $type;
        $this->message = $message . "; correlation_id:$correlationId" ;
    }
} 