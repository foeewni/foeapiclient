<?php
/**
 * Created by PhpStorm.
 * User: simon.phillips
 * Date: 24/04/15
 * Time: 08:51
 */

namespace Foe\Api\Client;


class FoeApiInvalidInputException extends FoeApiException{

    protected $input_errors;

    public function __construct($type, $message, $inputErrors, $correlationId)
    {
        $this->input_errors = $inputErrors;
        parent::__construct($type, $message, $correlationId);
    }

    /**
     * Get the errors with the input
     * @return array
     */
    public function getInputErrors()
    {
        return $this->input_errors;
    }
} 