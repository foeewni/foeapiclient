<?php
/**
 * User: Rich Lott
 * Date: 30 July 2015
 */

namespace Foe\Api\Client\Modules;


class Payments extends BaseModule {

    /**
     * Send data required by the API for taking a payment.
     * @param array $data
     *
     * @return mixed
     */
    public function takeCardPayment($data)
    {
        // This is useful: return $this->api->doPost("payments/card/payments?XDEBUG_SESSION_START=vim", $data);
        return $this->api->doPost("payments/card/payments", $data);
    }
    /**
     * Send data required by the API for recording a payment.
     * @param array $data
     *
     * @return mixed
     */
    public function recordPayment($data)
    {
        return $this->api->doPost("payments/card/records", $data);
    }
    /**
     * Send data required by the API for sending a thank you email.
     * @param array $data
     *
     * @return mixed
     */
    public function sendThanks($data)
    {
        return $this->api->doPost("payments/card/thanks", $data);
    }
    /**
     * Send data required by the API to take payment, record payment and send thanks.
     * @param array $data
     *
     * @return mixed
     */
    public function payRecordThanks($data)
    {
        return $this->api->doPost("payments/card/pay-record-thanks", $data);
    }

    /**
     * Send data required by the API to take a direct debit, record it and send thanks.
     * @param array $data
     *
     * @return mixed
     */
    public function registerRecordThank($data)
    {
        return $this->api->doPost("payments/direct-debit/register-record-thank", $data);
    }

    /**
     * Get the client token from the payments provider
     * @param array $data
     * @return array 'clientToken': the client token string
     */
    public function getClientToken($data)
    {
        return $this->api->doPost("payments/card/get-client-token", $data);
    }
}