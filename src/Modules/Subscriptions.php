<?php
/**
 * Created by PhpStorm.
 * User: simon.phillips
 * Date: 24/04/15
 * Time: 08:59
 */

namespace Foe\Api\Client\Modules;


class Subscriptions extends BaseModule{

    /**
     * @param array $params
     * sourcecode
     * title
     * postcode
     * address1
     * address2
     * town
     * country
     * first_name*
     * last_name*
     * email*
     * app [enews|ee_neww|em_news|londnews|nw_news|nw_news|se_news|sw_news|wm_news|yh_news]
     * @return mixed
     */
    public function subscribe(array $params) {
        return $this->api->doPost("subscriptions/subscribe", $params);
    }

    /**
     * Unsubscribe an email
     * @param $email
     * @param $firstName
     * @param $lastName
     * @param $emailProduct
     * @return mixed
     */
    public function unsubscribe($email, $firstName, $lastName, $emailProduct) {
        $params = array(
            'email' => $email,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email_product' => $emailProduct
        );

        return $this->api->doPost("subscriptions/subscribe", $params);
    }
} 