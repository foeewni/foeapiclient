<?php
/**
 * Created by PhpStorm.
 * User: simon.phillips
 * Date: 24/04/15
 * Time: 08:59
 */

namespace Foe\Api\Client\Modules;


class Groups extends BaseModule{

    /**
     * Fetch the current list of groups
     * @return mixed
     */
    public function getList()
    {
        return $this->api->doGet('groups/');
    }

    /**
     * Fetch details for a given group
     * @param int $id
     * @return mixed
     */
    public function getGroup($id)
    {
        return $this->api->doGet("groups/$id");
    }

    /**
     * Fetch the five groups nearest a given postcode
     * @param $postcode
     * @return mixed
     */
    public function getGroupNearestPostcode($postcode)
    {
        return $this->api->doGet("groups/nearest/postcode/$postcode");
    }
} 