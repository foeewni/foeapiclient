<?php
/**
 * Created by PhpStorm.
 * User: simon.phillips
 * Date: 24/04/15
 * Time: 08:59
 */

namespace Foe\Api\Client\Modules;


class Status extends BaseModule{

    public function heartbeat()
    {
        return $this->api->doGet("status/heartbeat");
    }

    public function version()
    {
        return $this->api->doGet("status/version");
    }

} 