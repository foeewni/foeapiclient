<?php
/**
 * Created by PhpStorm.
 * User: simon.phillips
 * Date: 24/04/15
 * Time: 08:14
 */

namespace Foe\Api\Client;


use Foe\Api\Client\Modules\CampaignActions;
use Foe\Api\Client\Modules\Groups;
use Foe\Api\Client\Modules\Subscriptions;
use Foe\Api\Client\Modules\Payments;
use Foe\Api\Utils;

class FoeApi {

    private $baseUrl;
    private $environmentOverride;
    private $apiToken;
    private $httpRequester;

    public $groups;
    public $campaignactions;
    public $payments;
    public $subscriptions;

    const API_TOKEN = 'X-FOE-API-TOKEN';
    const OVERRIDE_ENV_HEADER = 'FOE_OVERRIDE_ENV';
    const USERAGENT = 'FoeApiClient';

    public function __construct($baseUrl, $apiToken = '', $environmentOverride = false)
    {
        // We want a trailing slash on the base Url.
        if (substr($baseUrl,-1,1) != '/') $baseUrl .= '/';

        $this->baseUrl = $baseUrl;
        $this->apiToken = $apiToken;
        $this->environmentOverride = $environmentOverride;
        $this->httpRequester = new \Requests_Session(
          $this->baseUrl,
          $this->buildHeaders(),
          array(),
          array(
            'useragent' => self::USERAGENT,
            'timeout' => 90
          ));

        // Build the submodules
        $this->groups = new Groups($this);
        $this->campaignactions = new CampaignActions($this);
        $this->subscriptions = new Subscriptions($this);
        $this->payments = new Payments($this);
    }

    private function buildHeaders()
    {
        $headers = array(
            self::API_TOKEN => $this->apiToken
        );
        if ($this->environmentOverride)
            $headers[self::OVERRIDE_ENV_HEADER] = $this->environmentOverride;

        return $headers;
    }

    /**
     * Issue a stateless request against the Api
     * @param $urlPath
     * @return mixed
     */
    public function doGet($urlPath)
    {
        $queryUrl = $this->baseUrl . $urlPath;
        $response = $this->httpRequester->get($queryUrl);

        return $this->processResponse($response);
    }

    /**
     * Issue a stateful (ie data-changing) request against the Api
     * @param $urlPath
     * @param array $params
     * @return mixed
     */
    public function doPost($urlPath, array $params)
    {
        $queryUrl = $this->baseUrl . $urlPath;
        $data = json_encode($params);

        $response = $this->httpRequester->post($queryUrl,array(),$data);
        return $this->processResponse($response);
    }

    /**
     * Process an API response, throwing an exception if there was a server-side or connection error
     * @param \Requests_Response $response
     * @throws FoeApiException
     * @throws FoeApiInvalidInputException
     * If there's an issue with the input passed to the API
     * @return array
     */
    private function processResponse(\Requests_Response $response)
    {
        $body = json_decode($response->body,true);

        if ($response->success) return $body;

        if ($body == null) $body = array();

        $error_type = array_key_exists('error_type', $body) ? $body['error_type'] : $response->status_code;
        $message = array_key_exists('error',$body) ? $body['error'] : '';
        $correlationId = array_key_exists('correlation_id', $body) ? $body['correlation_id'] : '';

        if ($error_type == 'input') {
            $input_errors = array_key_exists('input_errors',$body) ? $body['input_errors'] : array();
            throw new FoeApiInvalidInputException($error_type, $message, $input_errors, $correlationId);
        }

        throw new FoeApiException($error_type,$message,$correlationId);
    }

} 
